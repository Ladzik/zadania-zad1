# -*- coding: utf-8 -*-

import socket
import sys
import socket
import logging
import logging.config

def menu(logger):
    while True:
        try:
            a = raw_input("podaj x ")
            b = raw_input("podaj y ")
            print "suma = ", client("" + a + " " + b, logger)

        except KeyboardInterrupt:
            sys.exit()

def client(message, logger):
    server_address = ('localhost', 12345)

    # Tworzenie gniazda TCP/IP
    s = socket.socket()

    # Polaczenie z gniazdem nasluchujacego serwera
    #logger.info(u'nawiazuje polaczenie z {0} na porcie {1}'.format(*server_address))
    s.connect(server_address)
    try:
        # Wyslanie danych
        #logger.info(u'wysylam "{0}"'.format(message))
        msg = message
        s.sendall(msg)

        # Odebranie odpowiedzi
        received = ''
        buffsize = 4096
        done = False

        while not done:
            part = s.recv(buffsize)
            if len(part) < buffsize:
                done = True
                received += part

        #logger.info(u'odebrano "{0}"'.format(received))

    finally:
        # Zamkniecie polaczenia na zakonczenie dzialania
        s.close()
        #logger.info(u'polaczenie zostalo zakonczone')

    return received

if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('sum_client')
    menu(logger)