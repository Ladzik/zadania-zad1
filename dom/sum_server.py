# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def suma(a, b):
    return str(float(a) + float(b))


def server(logger):
    server_address = ('localhost', 12345)
    # Tworzenie gniazda TCP/IP
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(server_address)

    logger.info(u'tworze serwer na {0}:{1}'.format(*server_address))
    s.listen(1)
    try:
        while True:
            logger.info(u'czekam na połączenie')
            connection, address = s.accept()
            addr = ('', '')
            addr = address

            try:
                # Odebranie danych
                buffsize = 4096
                done = False
                data = ''
                while not done:
                    part = connection.recv(buffsize)
                    if len(part) < buffsize:
                        done = True
                        data += part

                # Odesłanie odebranych danych spowrotem
                try:
                    a, b = data.split()
                    connection.sendall(str(suma(a, b)))

                except Exception, e:
                    connection.sendall("Blad")

            finally:
                # Zamknięcie połączenia
                connection.close()
                logger.info(u'zamknięto połączenie')

    except KeyboardInterrupt:
        s.close()

if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('sum_server')
    server(logger)
    sys.exit(0)