# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def client(message, logger):
    """ Klient wysylajacy wiadomosc do serwera echo
    message - wiadomosc wysylana do serwera
    logger - mechanzm do logowania wiadomosci
    """
    server_address = ('localhost', 12345)  # TODO: zmienic port!

    # Tworzenie gniazda TCP/IP
    # TODO: wstawic kod tworzacy nowe gniazdo sieciowe
    s = socket.socket()

    # Polaczenie z gniazdem nasluchujacego serwera
    logger.info(u'nawiazuje polaczenie z {0} na porcie {1}'.format(*server_address))
    # TODO: polaczyc sie z gniazdem serwera

    s.connect(server_address)
    try:
        # Wyslanie danych
        logger.info(u'wysylam "{0}"'.format(message))
        # TODO: wyslac wiadomosc message do serwera
        msg = message
        s.sendall(msg)

        # Odebranie odpowiedzi
        # TODO: odebrac odpowiedz z serwera i zapisac ja w zmiennej received
        received = ''
        bufor = 2048
        valid = False

        while not valid:
            part = s.recv(bufor)
            if len(part) < bufor:
                valid = True
                received += part

        logger.info(u'odebrano "{0}"'.format(received))

    finally:
        # Zamkniecie polaczenia na zakonczenie dzialania
        # TODO: zamknac polaczenie sieciowe
        s.close()
        logger.info(u'polaczenie zostalo zakonczone')


if __name__ == '__main__':

    message = u'blabla'
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('echo_client')
    client(message, logger)